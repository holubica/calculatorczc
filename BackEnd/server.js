/* eslint-disable no-undef */
import express from "express";
import cors from "cors";
import bodyParser from "body-parser";

const app = express();

app.use(bodyParser.json({}));
app.use(cors());

const AMD_PRICE = 2000;
const INTEL_PRICE = 1000;
const INSURANCE_PRICE = 2000;
const INSURANCE_PRICE_NOT_INCLUDED = 0;

const calculate = (ram, diagonal, processor, isInsurance) => {
  const insuranceValue = isInsurance
    ? INSURANCE_PRICE
    : INSURANCE_PRICE_NOT_INCLUDED;
  let processorValue;
  switch (processor) {
    case "AMD":
      processorValue = AMD_PRICE;
      break;
    case "Intel":
      processorValue = INTEL_PRICE;
      break;
  }

  const totalAmount = Number(ram * diagonal * processorValue + insuranceValue);

  return { totalAmount: totalAmount };
};

app.post("/vypocet", (req, res) => {
  const ram = Number(req.body.ram);
  const diagonal = Number(req.body.diagonal);
  const processor = req.body.processor;
  const isInsurance = Boolean(req.body.isInsurance);
  const result = calculate(ram, diagonal, processor, isInsurance);
  res.set("Content-Type", "application/json");
  res.send(result);
});

app.listen(8000, () => {
  console.log("The calculation is performed in the port 8000.");
});
