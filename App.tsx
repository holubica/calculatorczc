import React from "react";
import {
  View,
  TouchableWithoutFeedback,
  Keyboard,
  StyleSheet,
} from "react-native";
import colors from "./colors";

import { Header } from "./components/Header";
import { Parameters } from "./components/Parameters";

export default function App() {
  const hideKeyboard = () => {
    Keyboard.dismiss();
  };
  return (
    <TouchableWithoutFeedback onPress={hideKeyboard}>
      <View style={styles.mainContainer}>
        <Header />
        <Parameters />
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
});
