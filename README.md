# Calculator

I created an online calculator that calculates the computer price based on input parameters (RAM size, monitor diagonal size, processor type and insurance).

This project was bootstrapped with [React Native Expo CLI](https://github.com/facebook/react-native), using [Typescript](https://www.typescriptlang.org/), [Formik](https://formik.org/docs/overview) and [Yup](https://github.com/jquense/yup).

## Start project

For the project to run you need to have Expo-cli and Android studio (Windows and macOS) or Xcode (macOS only).

In the project directory, you can run:

#### `npm run start`

You also need to run the backend part. Navigate yourself to folder BackEnd and run:

#### `npm run start`
