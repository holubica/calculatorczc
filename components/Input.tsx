import React from "react";
import { View, TextInput, TextInputProps, StyleSheet } from "react-native";

export const Input = (props: TextInputProps) => {
  return (
    <View>
      <TextInput
        {...props}
        style={styles.input}
        autoComplete="off"
        autoCorrect={false}
        keyboardType="numeric"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    padding: 10,
    borderWidth: 1,
  },
});
