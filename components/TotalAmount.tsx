import React from "react";
import { View, Text, ActivityIndicator, StyleSheet } from "react-native";

import colors from "../colors";

export const TotalAmount = (props: {
  totalAmount: number | undefined;
  isLoading: boolean;
}) => {
  return (
    <View>
      <Text style={styles.totalAmountText}>
        Cena počítače:{" "}
        {props.isLoading ? (
          <ActivityIndicator size="small" color={colors.primary} />
        ) : (
          props.totalAmount
        )}{" "}
        Kč
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  totalAmountText: {
    fontSize: 15,
    marginBottom: 10,
  },
});
