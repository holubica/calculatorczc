import React, { useEffect, useState } from "react";
import { View, Text, Button, Switch, StyleSheet } from "react-native";

import { Picker } from "@react-native-picker/picker";
import { Formik } from "formik";
import Tooltip from "rn-tooltip";
import * as yup from "yup";

import colors from "../colors";
import { TotalAmount } from "./TotalAmount";
import { Input } from "./Input";
import { URL_ANDROID } from "../Config";

const ParametersSchema = yup.object({
  ram: yup.string().required("Toto je povinné pole."),
  diagonal: yup.string().required("Toto je povinné pole."),
  processor: yup.string().required("Toto je povinné pole."),
});

// RegEx that let you write only numbers
const regEx = /[^0-9]/g;

// RegEx that let you write only numbers, comma and dot
const regExWithDots = /[^0-9,.]/g;

export const Parameters = () => {
  const initialParameters = {
    ram: "",
    diagonal: "",
    processor: "",
    isInsurance: false,
  };
  const [parameters, setParameters] = useState(initialParameters);
  const [totalAmount, setTotalAmount] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      ram: parameters.ram,
      diagonal: parameters.diagonal,
      processor: parameters.processor,
      isInsurance: parameters.isInsurance,
    }),
  };

  useEffect(() => {
    const responseFromBE = async () => {
      try {
        setIsLoading(true);
        const response = await fetch(URL_ANDROID, requestOptions);
        const data = await response.json();
        setTotalAmount(data.totalAmount);
      } catch (error) {
        alert(error);
      }
      setIsLoading(false);
    };
    responseFromBE();
  }, [parameters]);

  return (
    <View style={styles.parametersContainer}>
      <TotalAmount totalAmount={totalAmount} isLoading={isLoading} />
      <Formik
        initialValues={initialParameters}
        validationSchema={ParametersSchema}
        onSubmit={(values) => {
          setParameters((prevState) => ({
            ...prevState,
            ram: values.ram,
            diagonal: values.diagonal,
            processor: values.processor,
            isInsurance: values.isInsurance,
          }));
        }}
      >
        {(props) => (
          <View style={{ flex: 1 }}>
            <Input
              editable={!isLoading}
              placeholder="RAM *"
              onChangeText={props.handleChange("ram")}
              value={props.values.ram.replace(regEx, "")}
              onBlur={props.handleBlur("ram")}
            />
            <Text style={styles.errorText}>
              {props.touched.ram && props.errors.ram}
            </Text>
            <Input
              editable={!isLoading}
              placeholder="Úhlopříčka *"
              onChangeText={props.handleChange("diagonal")}
              value={props.values.diagonal.replace(regExWithDots, "")}
              onBlur={props.handleBlur("diagonal")}
            />
            <Text style={styles.errorText}>
              {props.touched.diagonal && props.errors.diagonal}
            </Text>
            <View style={styles.pickerComponent}>
              <Picker
                enabled={!isLoading}
                mode="dropdown"
                selectedValue={props.values.processor}
                onValueChange={(value) => {
                  props.setFieldValue("processor", value);
                }}
                style={styles.picker}
              >
                <Picker.Item label="Procesor" style={{ color: "grey" }} />
                <Picker.Item label="AMD Ryzen 5 5600" value="AMD" />
                <Picker.Item label="Intel Core i5-11600K" value="Intel" />
              </Picker>
              <Text style={styles.errorTextPicker}>
                {props.touched.processor && props.errors.processor}
              </Text>
            </View>
            <View style={styles.switchContainer}>
              <Switch
                disabled={isLoading}
                trackColor={{ false: colors.tertiary, true: colors.primary }}
                value={props.values.isInsurance}
                onValueChange={(value) =>
                  props.setFieldValue("isInsurance", value)
                }
              />
              <Tooltip
                actionType="press"
                height={100}
                width={200}
                popover={
                  <Text>
                    Se službou Pojištění proti rozbití či krádeži jste v suchu,
                    ať se s vaším zařízením stane cokoli.
                  </Text>
                }
              >
                <Text>Pojištění ?</Text>
              </Tooltip>
            </View>
            <Button
              disabled={isLoading}
              title="Spočítat"
              color={colors.primary}
              onPress={props.handleSubmit}
            />
          </View>
        )}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  parametersContainer: {
    flex: 1,
    margin: 10,
  },
  text: {
    fontSize: 20,
  },
  pickerComponent: {
    height: 40,
    padding: 10,
    borderWidth: 1,
    marginBottom: 30,
  },
  picker: {
    height: 40,
    marginTop: -18,
    marginLeft: -10,
    width: "105%",
  },
  button: {
    flex: 1,
    marginTop: 0,
    marginHorizontal: 15,
    marginBottom: 20,
    textTransform: "capitalize",
    justifyContent: "flex-end",
  },
  switchContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 12,
  },
  errorText: {
    color: colors.primary,
    marginBottom: 12,
    fontSize: 12,
  },
  errorTextPicker: {
    marginBottom: 0,
    color: colors.primary,
    marginLeft: -10,
    fontSize: 12,
  },
});
