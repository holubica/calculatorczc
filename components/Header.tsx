import React from "react";
import { View, Text, StyleSheet, Platform, StatusBar } from "react-native";
import colors from "../colors";

export const Header = () => {
  return (
    <View style={styles.headerContainer}>
      <Text style={styles.headerText}>CZC</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: colors.primary,
    height: 120,
    justifyContent: "center",
    marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  headerText: {
    marginLeft: 10,
    fontSize: 50,
    color: colors.secondary,
    fontWeight: "bold",
  },
});
